//package Project2;

import java.util.ArrayList;
/**
 * Write a description of class Branch here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Branch
{
    protected String branchNum;
    protected String location;
    protected String contactNum;
    public Branch(String branchNum, String location, String contactNum)
    {
       this.branchNum = branchNum;
       this.location = location;
       this.contactNum = contactNum;
    }
    
    public String getBranchNum(){
        return this.branchNum;
    }
    public int equals(Branch a)
    {
         int c=0;
         if((branchNum==a.branchNum) && (location.equals(a.location))){
			  c++;
		 }
	     return c;      
    }
   
    
    public String toString()
    {
        String disp="Branch number: "+branchNum;
		disp+="\n         Location: "+location;
		disp+="\n   Contact number: "+contactNum+"\n";
		return disp;
    }
}