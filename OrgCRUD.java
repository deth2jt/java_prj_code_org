
/**
 * Write a description of class OrgCRUD here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */

import java.io.File;  // Import the File class
import java.io.FileNotFoundException;  // Import this class to handle errors
import java.util.Scanner; // Import the Scanner class to read text files
import java.io.IOException;  // Import the IOException class to handle errors
import java.io.FileWriter;   // Import the FileWriter class
import java.util.ArrayList;


public class OrgCRUD extends AbstractCRUD
{
    String delimiter = "\t";

    public OrgCRUD(String fileName)
    {
        // initialise instance variables
        super(fileName) ;
        createFile(fileName);
        super.listOfRecords = new ArrayList<Record>();
        //x = 0;
    }

    public void createFile(String fileName)
    {
        try {
            //File myObj = new File(fileName);
            if (new File(fileName).createNewFile()) {
              System.out.println("File created: ");
            } else {
              System.out.println("File already exists.");
            }
          } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
          }
    }

    public  int saveData()
    {
        int x = 0;
        try
        {
            FileWriter myWriter = new FileWriter(super.fileName);
            //System.out.println("super.listOfRecords.size()super.listOfRecords.size(): " + super.listOfRecords.size());
            for(x = 0; x < super.listOfRecords.size(); x++)
            {
                Organization org = (Organization) super.listOfRecords.get(x);
                myWriter.write(org.getregNum() + delimiter + org.getName() + delimiter + org.getbizType());
                if(x < super.listOfRecords.size() -1)
                    myWriter.write("\n");
            }
            //myWriter.write("Files in Java might be tricky, but it is fun enough!");
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } 
        catch (IOException e) 
        {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return x;
    }
    public  void createRecord(Record r)
    {
        super.listOfRecords.add(r);
    }

    public Record retrieveRecord(String key)
    {
        Record org;
        int x;
        for(x = 0; x < super.listOfRecords.size(); x++)
        {
            org = (Organization) super.listOfRecords.get(x);
            //System.out.println("org.getKey(): " +  org.getKey() + "x:" + x + " super.listOfRecords.size(): " + super.listOfRecords.size());
            if(org.getKey().equals(key))
            {
                //System.out.println("ALIEV");
                return org;
                
            }
        }
        return null;
    }

    public boolean updateRecord(Record r)
    {
        Record rec = (Organization) retrieveRecord(r.getKey());
        //System.out.println("recrecrecrec: " +  rec);

        if( rec != ( null))
        {            
            //rec = (Organization) r;
            //rec =  new Organization(r.getregNum(), r.getName(), r.getbizType());
            super.listOfRecords.add( super.listOfRecords.indexOf(rec) , r);
            return true;
        }
        else
            return false;
    }

    public boolean deleteRecord(String key)
    {
        Record rec = (Organization) retrieveRecord(key);
        if(! rec.equals( null))
        {            
            super.listOfRecords.remove(rec);
            return true;
        }
        else
            return false;
    }

    /*
    this.regNum = regNum;
    this.name = name;
    this.bizType = bizType;
    regNum name bizType
    */

    public int loadDataFile() 
    {
        //return 0;
        int count = 0;
        try 
        {
            super.listOfRecords.clear();
            //Organization[] organization = new Organization[countLine(super.fileName)];
            //File myObj = new File("filename.txt");
            Scanner myReader = new Scanner(new File(super.fileName));
            
            while (myReader.hasNextLine()) 
            {
              String data = myReader.nextLine();
              

                Scanner sc=new Scanner(data);  
                //while (sc.hasNext())   
               // {  
                    //invoking next() method that splits the string   
                    String regNum=sc.next(); 
                    String name=sc.next();  
                    String bizType=sc.next(); 
                    //prints the separated tokens  
                    //System.out.println(tokens);  
                    //closing the scanner  
                    sc.close();  
               // }  
                Record rec = new Organization(regNum, name, bizType);
                super.listOfRecords.add(rec);
                //System.out.println("recrecrecrecrecrec: " +rec);
                count++;
            }
            myReader.close();
          } 
          catch (FileNotFoundException e) 
          {
            System.out.println("An error occurred.");
            e.printStackTrace();
          }
          return count;
    }
    

    public int countLine(String fileName) throws FileNotFoundException
    {
        int count = 0;
        try 
        {
            //File myObj = new File("filename.txt");
            Scanner myReader = new Scanner(new File(super.fileName));
            while (myReader.hasNextLine()) 
            {
              String data = myReader.nextLine();
              //System.out.println(data);
              count++;
            }
            myReader.close();
          } 
          catch (FileNotFoundException e) 
          {
            System.out.println("An error occurred.");
            e.printStackTrace();
          }
          return count;
    }

    
}
