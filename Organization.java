//package Project2;
import java.util.ArrayList;

/**
 * Abstract class Organization - write a description of the class here
 * 
 * @author (your name here)
 * @version (version number or date here)
 */
//public abstract class Organization implements Record
public class Organization implements Record
{
    protected String regNum;
    protected String name;
    protected String bizType;
    protected ArrayList<Branch> branch = new ArrayList<Branch>();
    protected static int countBank;
    protected static int countSch;
    
    /**
     * Constructor 1 intializing the registration number, name and type of business 
     */
    public Organization(String regNum, String name, String bizType)
    {
        this.regNum = regNum;
        this.name = name;
        this.bizType = bizType;
    }
    
    public void addBranch(Branch obj)
     {
      branch.add(obj);
    }
    public String getBranches(){
        String disp="";
		for (int i=0;i<branch.size();i++){
		     disp+=branch.get(i).toString()+"\n";
		}
		return disp;
    }
    
    public String getregNum()
    {
        return this.regNum;
    }
    public String getName()
    {
        return this.name;
    }
    public String getbizType()
    {
        return this.bizType;
    }
    
    
    
    
    public boolean equals(Organization x)
    {
        boolean c=false;
		if(this.regNum.equals(x.getregNum())){
			c=true;
		}
		return c;
    }
    
    public int getIncBank(){
        return countBank;
    }
   
    
    public int getIncSchool(){
        return countSch;
    }
    
    public int compareTo(Organization other){
        int c=0;
	    if(regNum.compareTo(other.regNum) == -1){
			c=-1;
		}
		else 
		{
			c=1;
		}
		return c;
    }
    
    //definition of the method in the record 
    //@Override
    public String getKey()
    {
        return regNum;
    }
    
    
    public String toString()
    {
        return regNum + ", " + name + ", " + bizType;
    }
}


