
/**
 * Write a description of class driver here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class driver
{
    public static void main(String[] args)
    {
        AbstractCRUD orgCrud = new OrgCRUD("foofile.txt");

        Record rec1 = new Organization("regNum01", "name01",  "bizType01");
        orgCrud.createRecord(rec1);
        Record rec2 = new Organization("regNum02", "name02",  "bizType02");
        orgCrud.createRecord(rec2);
        Record rec3 = new Organization("regNum03", "name03",  "bizType03");
        orgCrud.createRecord(rec3);

        System.out.println("orgCrud.getAllRecords(): " +  orgCrud.getAllRecords());
        System.out.println("orgCrud: " +  orgCrud);

        System.out.println("orgCrud.saveData() " + orgCrud.saveData());
        System.out.println("orgCrud.loadDataFile() " + orgCrud.loadDataFile());
        System.out.println("orgCrud.saveData() "+ orgCrud.saveData());

        //if
        
        System.out.println("bitte da druben hier" +  orgCrud.retrieveRecord("regNum01"));

        Record rec4 = new Organization("regNum01", "name666",  "bizType666");
        orgCrud.updateRecord(rec4);

        System.out.println("jetzt records ist: " +  orgCrud.retrieveRecord("regNum01"));

        orgCrud.deleteRecord("regNum02");
        orgCrud.saveData();
        System.out.println("orgCrud: " +  orgCrud);
        

    }
}
